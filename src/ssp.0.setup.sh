################################################################################
# (c) 2015-2017 Merly Escalona <merlyescalona@uvigo.es>
# Phylogenomics Lab. University of Vigo.
#
# Description:
# ==============================================================================
# Pipeline
################################################################################
#!/bin/bash -l
################################################################################
# Program paths
################################################################################
pipelinesName="ssp"
WD="$HOME/research/sim-shallowphy"
outputFolder="$WD/output"
usageFolder="$WD/usage"
infoFolder="$WD/info"
scriptsFolder="$WD/git/src"
jobsSent="$infoFolder/jobs.sent.txt"
filterEven="$WD/git/scripts/filtering.even.R"


matingProgram="$HOME/src/me-phylolab-mating/mating.py"
pipelineBash="$HOME/src/me-phylolab-conussim/conusSim-pipeline"
LociFiltering="$HOME/src/me-phylolab-mating/LociFiltering.py"
lociReferenceSelection="$HOME/src/me-phylolab-conussim/LociRefSelection/locirefsel.main.py"
art="$HOME/bin/art_illumina"
simphy="$HOME/bin/simphy"
indelible="$HOME/bin/indelible"
wrapper="$HOME/bin/INDELIble_wrapper.pl"
diversity="$HOME/bin/diversity"
fastqc="$HOME/apps/fastqc/0.11.5/fastqc"
bwa="$HOME/apps/bwa/0.7.13/bwa"
PICARD="$HOME/apps/picard/2.2.4/picard.jar"
GATK="$HOME/apps/gatk/3.5.0/GenomeAnalysisTK.jar"
VarScan="$HOME/apps/varscan/2.3.9/VarScan.v2.3.9.jar"
VarScan2="$HOME/apps/varscan/2.4.2/VarScan.v2.4.2.jar"
extractingmapq="$HOME/src/me-phylolab-conussim/mapq/extracting.mapq.R"
extractingvcftables="$HOME/src/me-phylolab-conussim/variants/extractVariants.multiple.R"
msa2vcf="$HOME/apps/jvarkit/1.0.0/dist/msa2vcf"
vcftools="$HOME/apps/vcftools/1.1.14/bin/vcftools"
freebayes="$HOME/apps/freebayes/1.0.0/bin/freebayes"
bcftools="$HOME/apps/bcftools/1.3.1/bcftools"
angsd="$HOME/apps/angsd/dd1bae7/angsd"
tabix="$HOME/apps/htslib/1.3.1/tabix"
bgzip="$HOME/apps/htslib/1.3.1/bgzip"
################################################################################
# Folders
################################################################################
PHYLOLAB="/home/uvi/be/mef/mnt/phylolab"
WD="$PHYLOLAB/$USER/$pipelinesName"
profilePath="$HOME/data/csNGSProfile"
pair1Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_1.txt"
pair2Path="$HOME/data/csNGSProfile/csNGSProfile_hiseq2500_2.txt"
outputFolder="$WD/report/output"
coverageFolder="$WD/report/coverage"
usageFolder="$WD/report/usage"
scriptsFolder="$WD/report/scripts"
filesFolder="$WD/report/files"
controlFolder="$WD/report/controlFiles"
jobsSent="$outputFolder/$pipelinesName.jobs"
readsFolder="$WD/reads"
consensusFolder="$WD/consensus"
individualsFolder="$WD/individuals"
alnReadsFolder="$WD/reads/aln"
fqReadsFolder="$WD/reads/fq"
samReadsFolder="$WD/reads/sam"
referencesFolder="$WD/references"
procSam="$WD/proc/sam"
procBam="$WD/proc/bam"
procSorted="$WD/proc/sorted"
procDup="$WD/proc/dup"
procMAPQ="$WD/proc/mapq"
qcFolder="$WD/report/qc"
stats="$WD/report/stats"
procVarcalling="$WD/proc/varcalling"
procIndMap="$WD/proc/indsorted"

################################################################################
# STEP 0. Setting environment
mkdir -p  $WD $outputFolder
$profilePath $outputFolder $usageFolder $scriptsFolder $filesFolder \
          $controlFolder $statsFolder $coverageFolder $readsFolder $fqReadsFolder \
          $alnReadsFolder $referencesFolder $samReadsFolder $qcFolder
          $procSam $procBam $procSorted $procDup $procMAPQ $procVarcalling $consensusFolder
