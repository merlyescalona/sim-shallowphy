#! /bin/bash
#$ -v pipelinesName=ssp
#$ -wd /home/merly/research/sim-shallowphy
#$ -o /home/merly/research/sim-shallowphy/output/ssp.1.3.filtering.sts.o
#$ -e /home/merly/research/sim-shallowphy/output/ssp.1.3.filtering.sts.e
#$ -N simphy.1.3
#$ -hold_jid simphy.1.1

module load R/3.2.2_1

Rscript git/scripts/filtering.even.R "${pipelinesName}/${pipelinesName}.db" "git/files/${pipelinesName}.evens" "git/files/${pipelinesName}.odds"

for line in $(cat git/files/${pipelinesName}.evens); do
  st=$(printf "%05g" ${line})
  echo "${st}" >> "git/files/${pipelinesName}.evens.jobs"
done

module unload R/3.2.2_1
