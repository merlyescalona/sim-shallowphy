#! /bin/bash
#$ -v pipelinesName=ssp
#$ -wd /home/merly/research/sim-shallowphy/
#$ -o /home/merly/research/sim-shallowphy/output/ssp.1.1.simphy.o
#$ -e /home/merly/research/sim-shallowphy/output/ssp.1.1.simphy.e
#$ -N simphy.1.1

RS="20000" #number of species trees
RL="U:100,5000" # Number of locus tree /= to number of gene tress (1 locus tree per gene tree)
SB="LN:-13.58,1.85" # Speciation rate - depends on SU and SI (species tree height and number of inds. per taxa/tips)
SU="U:0.00000001,0.0000000001" # Mutation rate (10e-8 - 10e-10)
ST="U:200000,20000000"
SL="U:4,20" #Num. taxa
SI="U:2,20" #numIndTaxa
SO="F:1" # outgroupBranch length - This can be modified (LN:0,1)
SG="F:1" # tree wide generation time
GP="LN:1.4,1"  # Gene-by-lineage-specific rate heterogeneity modifier (HYPER PARAM)
HH="LN:1.2,1" # Gene-by-family heterogeneity
HG="F:GP" # Gene-by-lineage-specific rate heterogeneity modifier
SP="F:10000" # Effective population size

module load bio/simphy/1.0.2
simphy -rs $RS -rl $RL -su $SU -sb $SB -sl $SL -si $SI -sp $SP -st $ST -so $SO -sg $SG -gp $GP -hh $HH -hg $HG  -v 1 -o $pipelinesName -cs $RANDOM -om 1 -od 1 -op 1 -oc 1 -on 1
module unload bio/simphy/1.0.2
