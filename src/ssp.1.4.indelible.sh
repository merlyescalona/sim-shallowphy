#! /bin/bash
#$ -v pipelinesName=ssp
#$ -wd /home/merly/research/sim-shallowphy
#$ -o /home/merly/research/sim-shallowphy/output/ssp.1.4.indelible.o
#$ -e /home/merly/research/sim-shallowphy/output/ssp.1.4.indelible.e
#$ -N simphy.1.4
#$ -hold_jid simphy.1.1,simphy.1.2,simphy.1.3

SEED=$(awk "NR==${SGE_TASK_ID}" git/files/${pipelinesName}.evens.jobs)
echo ${pipelinesName}/${SEED}
cd ${pipelinesName}/${SEED}
module load bio/indelible/1.03
indelible
module load bio/indelible/1.03
