#! /bin/bash
#$ -v pipelinesName=ssp
#$ -wd /home/merly/research/sim-shallowphy
#$ -o /home/merly/research/sim-shallowphy/output/ssp.1.2.indelible.wrapper.o
#$ -e /home/merly/research/sim-shallowphy/output/ssp.1.2.indelible.wrapper.e
#$ -N simphy.1.2
#$ -hold_jid simphy.1.1

module load perl/5.22.1_1
controlFile="git/files/indelible.control.v2.txt"
wrapper="git/scripts/INDELible_wrapper_v2.pl"
#Usage: ./INDELIble_wrapper.pl directory input_config seed numberofcores
perl $wrapper $pipelinesName $controlFile $RANDOM 1 &> "output/${pipelinesName}.1.2.indelible.wrapper.txt"
module unload perl/5.22.1_1
